package kz.aitu.week6.tree;

public class Tree {
    private Node root;

    public Tree() {
        root = null;
    }

    private Node insertRec(Node temp, int key, String value) {
        if (temp == null) {
            return new Node(key, value);
        }
        if (key < temp.getKey()) {
            temp.setLeft(insertRec(temp.getLeft(), key, value));
        } else if (key > temp.getKey()) {
            temp.setRight(insertRec(temp.getRight(), key, value));
        } else {
            return temp;
        }

        return temp;
    }
    public void inorder(Node root){
        if (root!=null){
            inorder(root.getLeft());
            System.out.print(root.getValue() +" ");
            inorder(root.getRight());
        }
    }
    public void preorder(Node root){
        if (root!=null){
            System.out.print(root.getValue()+" ");
            preorder(root.getLeft());
            preorder(root.getRight());
        }
    }
    public void postorder(Node root){
        if (root!=null){
            postorder(root.getLeft());
            postorder(root.getRight());
            System.out.print(root.getValue()+" ");
        }
    }




    public void insert(int key, String value) {
        root = insertRec(root, key, value);
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }
}
