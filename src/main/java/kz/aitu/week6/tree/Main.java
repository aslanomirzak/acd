package kz.aitu.week6.tree;

public class Main {
    public static void main(String[] args) {
        Tree tree = new Tree();
        tree.insert(5, "Aslan");
        tree.insert(8, "Dimash");
        tree.insert(3, "Damir");
        tree.insert(1, "Sungat");
        tree.insert(4, "Erasyl");
        tree.insert(7, "Aidyn");
        tree.insert(9, "Alemkhan");
        tree.preorder(tree.getRoot());
    }
}
