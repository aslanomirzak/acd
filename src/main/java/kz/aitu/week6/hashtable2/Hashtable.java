package kz.aitu.week6.hashtable2;

public class Hashtable {
    private Node[] table;
    int size;

    public Hashtable(int size){
        table = new Node[size];
        this.size = size;
    }

    public void insert(String key, String value){
        Node node = new Node(key, value);
        if(table[key.hashCode() % size] == null){
            table[key.hashCode()% size] = node;
        } else{
            Node temp = table[key.hashCode() % size];
            while(temp != null){
                if(temp.getKey() == node.getKey()){
                    temp.setValue(node.getValue());
                    return;
                }
                if(temp.getNext() == null){
                    temp.setNext(node);
                    return;
                }
                temp = temp.getNext();
            }
        }
    }

    public void printHashTable(){
        for(int i=0; i < size; i++){
            if(table[i] == null){
                continue;
            }
            Node temp = table[i];
            while(temp!= null){
                System.out.println(temp.getValue() + " ");
                temp = temp.getNext();
            }
            System.out.println();
        }
    }
}
