package kz.aitu.week6.hashtable2;

public class Main {
    public static void main(String[] args) {
        Hashtable table1 = new Hashtable(2);
        table1.insert("one", "Aslan");
        table1.insert("two", "Aidyn");
        table1.insert("four", "Sungat");
        table1.insert("three","Damir");
        table1.insert("five","Erasyl");
        table1.insert("six","Alikhan");
        table1.printHashTable();
    }
}
