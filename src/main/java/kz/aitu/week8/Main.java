package kz.aitu.week8;

public class Main {

    public static void quickSort(int[] a, int start, int end){
        if (start<end) {
            int j = start - 1;
            for (int i = start; i < end; i++) {
                if (a[i] < a[end]) {
                    j++;
                    int temp = a[j];
                    a[j] = a[i];
                    a[i] = temp;
                }
            }
            j++;
            int temp = a[j];
            a[j] = a[end];
            a[end] = temp;
            quickSort(a, start, j-1);
            quickSort(a, j+1, end);
        } else return;
    }

    public static void findComb(int[] arr, int sum) {
        findComb(arr, sum, 0, arr.length - 1);
    }

    private static void  findComb(int[] arr, int sum, int start, int end) {
        boolean isTwo = false;
        while (start < end) {
            if (arr[start] + arr[end] == sum) {
                System.out.println("n1 = " + arr[start]);
                System.out.println("n2 = " + arr[end]);
                isTwo = true;
                start++;
                System.out.println();
            } else if (arr[start] + arr[end] > sum) {
                end--;
            } else {
                start++;
            }
        }
        boolean isThree = false;
        if(isTwo == false){
            for(int i = 0; i<arr.length-2;i++){
                for(int j = i+1; j<arr.length-1; j++){
                    for(int k = j+1; k<arr.length; k++){
                        if(arr[i]+arr[j]+arr[k] == sum){
                            System.out.println("n1 = " + arr[i]);
                            System.out.println("n2 = " + arr[j]);
                            System.out.println("n3 = " + arr[k]);
                            System.out.println();
                            isThree = true;
                        }
                    }
                }
            }
        }
        if(isThree == false){
            for(int i = 0; i<arr.length-3;i++){
                for(int j = i+1; j<arr.length-2; j++){
                    for(int k = j+1; k<arr.length-1; k++){
                        for(int l = k+1; l<arr.length;l++){
                            if(arr[i]+arr[j]+arr[k]+arr[l] == sum){
                                System.out.println("n1 = " + arr[i]);
                                System.out.println("n2 = " + arr[j]);
                                System.out.println("n3 = " + arr[k]);
                                System.out.println("n4 = " + arr[l]);
                                System.out.println();
                            }
                        }
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {1,2,3,4,5,6,7,8,9,10,11};
        int sum = 37;
        quickSort(arr, 0, arr.length-1);
        findComb(arr, sum);
    }
}
