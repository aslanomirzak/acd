package kz.aitu.week2;
import java.util.Scanner;
public class task703 {
    public void inverseWords(int n){
        if (n == 0) return;
        else {
            Scanner sc = new Scanner(System.in);
            String str = sc.next();
            inverseWords(n-1);
            System.out.println(str);
        }
    }
    public void run() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        inverseWords(n);
    }
}
