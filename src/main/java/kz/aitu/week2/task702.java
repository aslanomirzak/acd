package kz.aitu.week2;
import java.util.Scanner;
public class task702 {
    public void reverseArr(int n){
        if (n == 0)return ;
        else {
            Scanner s = new Scanner(System.in);
            int a = s.nextInt();
            reverseArr(n - 1);
            System.out.println(a + " ");
        }
    }
    public void run(){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        reverseArr(n);
    }
}
