package kz.aitu.week2;
import java.util.Scanner;
public class task701 {
    public int fib(int a){
        if (a == 1 || a == 0) return a;
        return fib(a-2) + fib(a-1);
    }
    public void run() {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println(fib(n));
    }
}
