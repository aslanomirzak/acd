package kz.aitu.week4.queue;

public class Node {
    private Node next;
    private String data;

    public void setNext(Node next) {
        this.next = next;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Node getNext() {
        return next;
    }

    public String getData() {
        return data;
    }
}

