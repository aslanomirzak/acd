package kz.aitu.week4.queue;

public class Queue {
    private Node head;
    private Node tail;
    int size = 0;

    public void add (String data){
        Node newNode = new Node();
        if (size == 0){
            newNode.setData(data);
            newNode.setNext(null);
            head = newNode;
        } else {
            tail.setNext(newNode);
            newNode.setData(data);
            newNode.setNext(null);
        }
        tail = newNode;
        size++;
    }

    public String poll() {
        Node oldTop = head;
        head = head.getNext();
        size--;
        return oldTop.getData();
    }

    public void peek() {
        System.out.println(head.getData());
    }

    public int size(){
        return size;
    }
}
