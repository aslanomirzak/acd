package kz.aitu.week7.sort;

public class Quick {

    public void sort(int[] arr) {
        quickSort(arr,0,arr.length-1);
    }
    public void quickSort(int[] a, int start, int end){
        if (start<end) {
            int j = start - 1;
            for (int i = start; i < end; i++) {
                if (a[i] < a[end]) {
                    j++;
                    int temp = a[j];
                    a[j] = a[i];
                    a[i] = temp;
                }
            }
            j++;
            int temp = a[j];
            a[j] = a[end];
            a[end] = temp;
            quickSort(a, start, j-1);
            quickSort(a, j+1, end);
        } else return;
    }



}
