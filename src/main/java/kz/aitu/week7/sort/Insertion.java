package kz.aitu.week7.sort;

public class Insertion {

    public void sort(int[] arr) {
        for (int i = 1 ; i < arr.length; i++){
            int k = i;
            for (int j = i - 1; j>=0 ; j--){
                if (arr[k] < arr[j]) {
                    int temp = arr[k];
                    arr[k] = arr[j];
                    arr[j] = temp;
                    k--;
                } else{
                    break;
                }
            }
        }
    }
}
