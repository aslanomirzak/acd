package kz.aitu.week7.sort;

public class Selection {

    public void sort(int[] arr) {
        int minIndex = -1;
        int minValue = -1;
        for(int i = 0; i < arr.length; i++){
            minValue = arr[i];
            minIndex = i;
            for(int j = i; j < arr.length; j++){
                if(minValue>arr[j]){
                    minValue = arr[j];
                    minIndex = j;
                }
            }
            int temp = arr[i];
            arr[i] = minValue;
            arr[minIndex] = temp;

        }
    }
}
