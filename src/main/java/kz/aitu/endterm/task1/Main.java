package kz.aitu.endterm.task1;

public class Main {
    public static void main(String[] args) {
        Tree tree = new Tree(4);
        tree.addNode(2, tree.getRoot());
        tree.addNode(6, tree.getRoot());
        tree.addNode(1, tree.getRoot());
        tree.addNode(3, tree.getRoot());
        tree.addNode(5, tree.getRoot());
        tree.addNode(7, tree.getRoot());
        tree.addNode(9, tree.getRoot());
        System.out.println(tree.getRoot().getRight().getValue());
    }
}
