package kz.aitu.endterm.task1;

public class QueueNode {
    private Node node;
    private QueueNode next;

    public QueueNode(Node node) {
        this.node = node;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public QueueNode getNext() {
        return next;
    }

    public void setNext(QueueNode next) {
        this.next = next;
    }
}
