package kz.aitu.endterm.task1;

public class Queue {
    private QueueNode head;
    private QueueNode tail;

    public Queue(QueueNode head) {
        this.head = head;
        this.tail = head;
    }

    public void add(Node nnode){
        QueueNode node = new QueueNode(nnode);
        tail.setNext(node);
        tail = node;
    }

    public Node pop(){
        QueueNode node = head;
        head = head.getNext();
        return node.getNode();
    }

    public QueueNode getHead() {
        return head;
    }

    public void setHead(QueueNode head) {
        this.head = head;
    }

    public QueueNode getTail() {
        return tail;
    }

    public void setTail(QueueNode tail) {
        this.tail = tail;
    }
}
