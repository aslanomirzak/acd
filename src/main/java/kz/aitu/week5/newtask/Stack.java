package kz.aitu.week5.newtask;

public class Stack {
    private Node top;
    private int size = 0;
    public Node getTop(){
        return top;
    }

    public void push(int data){
        Node newNode = new Node();
        newNode.setData(data);
        if (size != 0) {
            newNode.setNext(top);
        }
        top = newNode;
        size++;
    }


    public int pop(){
        Node oldTop = top;
        top = top.getNext();
        size--;
        return oldTop.getData();
    }

    public boolean empty(){
        if (size == 0){
            return true;
        }else
            return false;
    }

    public int size(){
        return size;
    }

}
