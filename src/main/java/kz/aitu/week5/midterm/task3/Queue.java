package kz.aitu.week5.midterm.task3;

public class Queue {
    private Node head;
    private Node tail;

    public void push (String data){
        Node newNode = new Node();
        if (head == null){
            newNode.setData(data);
            newNode.setNext(null);
            head = newNode;
        } else {
            newNode.setData(data);
            newNode.setNext(null);
            tail.setNext(newNode);
        }
        tail = newNode;
    } // O(1)

    public void pop() {
        head = head.getNext();
    } //O(1)

    public void add(Node node){
        if(head == null){
            head = node;
        }
        else{
            tail.setNext(node);
        }
    } //O(1)

    public void printOdd(Node node){
        Node current = new Node();
        current = head;
        if(current == null){
            return;
        }
        else{
            if (current.getData().length() % 2 != 0) {
                System.out.print(current.getData() + " ");
            }
            printOdd(current.getNext());
        }

    }
}
