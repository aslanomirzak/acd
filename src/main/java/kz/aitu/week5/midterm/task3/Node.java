package kz.aitu.week5.midterm.task3;

public class Node {
    private Node next;
    private String data;

    public String getData() {
        return data;
    }

    public Node getNext() {
        return next;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
