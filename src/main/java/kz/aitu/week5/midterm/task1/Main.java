package kz.aitu.week5.midterm.task1;

public class Main {

    public static void main(String[] args) {
        Node node1 = new Node("Aslan");
        Node node2 = new Node("Alemkhan");
        Node node3 = new Node("Damir");
        Node node4 = new Node("Ulan");
        Node node5 = new Node("Temirkhan");

        LinkedList linkedList = new LinkedList();
        linkedList.push_back(node1);
        linkedList.push_back(node2);
        linkedList.push_back(node3);
        linkedList.push_back(node4);
        linkedList.push_back(node5);

        linkedList.printAll(node1);
    }

}
