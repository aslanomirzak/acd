package kz.aitu.week5.midterm.task1;

public class LinkedList {
    private Node head;
    private Node tail;

    public Node getTail() {
        return tail;
    }

    public Node getHead() {
        return head;
    }

    public void setHead(Node head) {
        this.head = head;
    }

    public void setTail(Node tail) {
        this.tail = tail;
    }

    public void push_back(Node node){
        if (head == null)
        {
            head = node;
            return;
        }
        node.setNext(null);
        Node last = head;
        while (last.next() != null)
            last = last.next();

        last.setNext(node);
    } //O(n)

    public void printAll(Node node){
        if (node == null)
            return;
        else {
            System.out.print(node.data()+ " ");
            printAll(node.next());
        }
    } // O(n)
}

