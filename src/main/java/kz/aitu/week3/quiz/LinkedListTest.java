package kz.aitu.week3.quiz;

public class LinkedListTest { // nothing to change


    public static void main(String args[]) {
        //creating LinkedList with 5 elements including head
        LinkedList linkedList = new LinkedList();

        linkedList.add( new Node("1"));
        linkedList.add( new Node("2"));
        linkedList.add( new Node("3"));
        linkedList.add( new Node("4"));
        linkedList.insertAt("CS1902", 2);
        linkedList.removeAt(1);
        linkedList.removeAt(0);
        Node head = linkedList.head();

        while(head!=null){
            System.out.println(head.data);
            head = head.next;
        }


    }

}
