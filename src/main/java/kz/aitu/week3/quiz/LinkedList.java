package kz.aitu.week3.quiz;

public class LinkedList{
    private Node head;
    private Node tail;

    public LinkedList(){
        this.head = new Node("head");
        tail = head;
    }

    public Node head(){
        return head;
    }

    public void add(Node node){
        tail.next = node;
        tail = node;
    }

    public void insertAt(String s, int index) {
        Node newNode = new Node(s);
        Node temp = head;
        if(index == 0){
            temp.setNext(head);
            head = temp;
        } else{
            temp = head;
            for(int i = 0; i < index-1; i++){
                temp = temp.next;
            }
            Node temp2 = head;
            for(int i = 0; i < index ; i++ ){
                temp2 = temp2.next;
            }
            temp.setNext(newNode);
            newNode.setNext(temp2);
        }
    }

    public void removeAt(int index) {
        Node temp = head;
        if(index == 0){
            head = head.next();
        } else {
            for (int i=0;i<index-1;i++){
                temp = temp.next;
            }
            Node temp2 = temp.next.next;
            temp.setNext(temp2);
        }
    }
}
