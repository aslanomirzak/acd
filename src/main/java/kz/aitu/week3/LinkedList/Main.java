package kz.aitu.week3.LinkedList;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Block block1 = new Block();
        Block block2 = new Block();
        Block block3 = new Block();
        Block block4 = new Block();
        Block block5 = new Block();
        Block block6 = new Block();

        block1.setData("one");
        block2.setData("two");
        block3.setData("three");
        block4.setData("four");
        block5.setData("five");
        block6.setData("six");

        block1.setNext(block2);
        block2.setNext(block3);
        block3.setNext(block4);
        block4.setNext(block5);
        block5.setNext(block6);
        block6.setNext(null);


        Block block7 = new Block();
        block7.setData("iphone");
        block2.setNext(block7);
        block7.setNext(block3);


        Block block8 = new Block();
        block8.setData("eight");
        block8.setNext(block1);

        Block block9 = new Block();
        block9.setData("nine");

        Block current2 = block8;
        while(current2.getNext() != null){
            current2 = current2.getNext();
        }
        current2.setNext(block9);

        Block add = new Block();
        add.setData("additional");
        add.insertIntoIndex(block8, add, 3);

        Block front = new Block();
        front.setData("front");
        front.addAtFront(block8, front);

        Block end = new Block();
        end.setData("end");
        end.addAtEnd(front, end);

        block8.printAllBlocks(front);
      //  System.out.println(block8.size(block8));

    }


}
