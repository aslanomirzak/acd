package kz.aitu.week3.LinkedList;

public class Block {
    private String data;
    private Block next;

    public Block getNext() {
        return next;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setNext(Block next) {
        this.next = next;
    }

    public static void printAllBlocks(Block head){
        Block current = head;
        while(head!=null){
            System.out.println(current.getData());
            current = current.getNext();
        }
    }

    public static int size(Block head){
        int counter = 0;
        while(head!=null){
            counter++;
            head = head.getNext();
        }
        return counter;
    }

    public static void insertIntoIndex(Block head, Block n7, int index){
        Block current = head;
        if (index == 0){
            n7.setNext(head);
        } else {
            for(int i = 0 ; i < index - 1; i ++){
                current = current.getNext();
            }
            n7.setNext(current.getNext());
            current.setNext(n7);
        }
    }
    public static void addAtFront(Block head, Block n){
        n.setNext(head);
    }

    public static void addAtEnd(Block head, Block n){
        Block current = head;
        while (current.getNext() != null){
            current = current.getNext();
        }
        current.setNext(n);
    }

}

