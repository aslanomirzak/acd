package kz.aitu.week1;

import java.util.Scanner;

public class Practice4 {
    public static int fact(int n){
        if (n == 1 || n == 0) {
            return 1;
        } else {
            return n*fact(n-1);
        }
    }

    public static void main(String[] args) {
        int n;
        Scanner cin = new Scanner(System.in);
        n = cin.nextInt();
        System.out.println(fact(n));
    }
}
