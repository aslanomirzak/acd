package kz.aitu.week1;
import java.util.Scanner;
public class Practice7 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int n;
        n = input.nextInt();
        int myArray[] = new int[n];

        for(int i = 0; i < n; i++){
            myArray[i] = input.nextInt();
        }

        reverse(myArray, 0, n-1);

        for(int i = 0; i < n; i++){
            System.out.println(myArray[i] + " ");
        }
    }
    public static void reverse(int myArray[], int start, int end) {
        int temp;
        if(start>=end) return;

        temp = myArray[start];
        myArray[start] = myArray[end];
        myArray[end] = temp;

        reverse(myArray, start+1, end-1);
    }
}
