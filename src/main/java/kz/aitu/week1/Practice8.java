package kz.aitu.week1;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Practice8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String number;
        number = sc.nextLine();
        if(isDigits(number) == true) {
            System.out.println("Yes");
        }else{
            System.out.println("No");
        }
    }
    public static boolean isDigits(String word){
        boolean a = true;
        for(int i= 0;i<word.length(); i++){
            a = Character.isDigit(word.charAt(i));
            if (a == false){
                break;
            }
        }
        return a;
    }
}
