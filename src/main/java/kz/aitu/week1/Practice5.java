package kz.aitu.week1;

import java.util.Scanner;

public class Practice5 {
    public static int fib(int n){
        if (n == 0 || n==1){
            return n;
        }else {
            return fib(n-2)+fib(n-1);
        }
    }

    public static void main(String[] args) {
        int n;
        Scanner cin = new Scanner(System.in);
        n = cin.nextInt();
        System.out.println(fib(n));
    }
}