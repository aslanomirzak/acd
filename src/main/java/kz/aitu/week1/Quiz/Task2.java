package kz.aitu.week1.Quiz;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        n = sc.nextInt();
        int max;
        max = n;
        System.out.println(series(n, max));
    }
    public static int series(int n, int max) {
        Scanner sc = new Scanner(System.in);
        if (n >= max) {
            max = n;
        }
        if (n==0){
            return max;
        } else {
            n = sc.nextInt();
            return series(n, max);
        }
}
}
