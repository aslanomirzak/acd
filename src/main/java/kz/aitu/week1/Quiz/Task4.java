package kz.aitu.week1.Quiz;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word;
        word = sc.nextLine();
        System.out.println(isPalin(word));
    }
    public static String isPalin(String word){
        if(word.length() == 1){
            System.out.println("YES");
        }
        if(word.substring(0, 1).equals(word.substring(word.length() - 1, word.length()))){
            if(word.length() == 2){
                return "YES";
            }
            return isPalin(word.substring(1, word.length() - 1));
        }else{
            return "NO";
        }
    }
}
