package kz.aitu.week1;
import java.util.Scanner;
public class practice10 {
    public static void main(String[] args){
        Scanner sc = new Scanner (System.in);
        int n, k;
        n = sc.nextInt();
        k = sc.nextInt();
        System.out.println(gcd(n,k));
    }
    public static int gcd(int n , int k){
        if (k!=0){
            return gcd(k, n%k);
        } else{
            return n;
        }
    }
}
