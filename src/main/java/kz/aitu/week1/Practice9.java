package kz.aitu.week1;

import java.util.Scanner;

public class Practice9 {
    public static void main(String[] args){
        Scanner sc = new Scanner (System.in);
        int n, k;
        n = sc.nextInt();
        k = sc.nextInt();
        System.out.println(binom(n, k));
    }
    public static int binom(int n, int k){
        if((k == 0) || (k == n)){
            return 1;
        } else {
            return (binom(n-1, k-1) + binom(n-1, k));
        }
    }
}
